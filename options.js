var siteList = document.getElementById("siteList");
var sites = document.getElementsByTagName("li");
var count = 0;

function save_options() 
{
  chrome.storage.sync.set({
    wipeList: document.getElementById("Input").value
  }, 
  function() {
  // Update status to let user know options were saved.
  var status = document.getElementById('status');
  status.textContent = 'Options saved.';
  setTimeout(function() 
  {
  status.textContent = '';
  }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.sync.get({
    wipeList = ""
  }, function(items) {
    document.getElementById("Input").value = items.wipeList;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);

document.getElementById("siteAdd").addEventListener("click", save_options);